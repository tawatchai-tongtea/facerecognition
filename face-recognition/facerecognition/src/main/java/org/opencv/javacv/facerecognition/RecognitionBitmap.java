package org.opencv.javacv.facerecognition;

import android.os.Parcel;
import android.os.Parcelable;

import org.opencv.core.Mat;

/**
 * Created by tawatchaitongtea on 3/29/2016 AD.
 */
public class RecognitionBitmap extends Mat implements Parcelable {


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public RecognitionBitmap() {
    }

    protected RecognitionBitmap(Parcel in) {
    }

    public static final Parcelable.Creator<RecognitionBitmap> CREATOR = new Parcelable.Creator<RecognitionBitmap>() {
        @Override
        public RecognitionBitmap createFromParcel(Parcel source) {
            return new RecognitionBitmap(source);
        }

        @Override
        public RecognitionBitmap[] newArray(int size) {
            return new RecognitionBitmap[size];
        }
    };
}
