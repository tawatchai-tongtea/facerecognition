package org.opencv.javacv.facerecognition;

import android.os.Parcel;
import android.os.Parcelable;

import com.orm.SugarRecord;

/**
 * Created by tawatchaitongtea on 3/30/2016 AD.
 */
public class User extends SugarRecord implements Parcelable {

    public String userName;
    public String name;
    public String lastName;
    public String bookNumber;
    public String passWord;
    public int idUser;
    public String image;

    public User() {

    }

    public User(String userName, String name, String lastName, String bookNumber, String passWord, int idUser,String image) {
        this.userName = userName;
        this.name = name;
        this.lastName = lastName;
        this.bookNumber = bookNumber;
        this.passWord = passWord;
        this.idUser = idUser;
        this.image = image;
    }

    public int getIdUser() {
        return idUser;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getUserName() {
        return userName;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public String getBookNumber() {
        return bookNumber;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setBookNumber(String bookNumber) {
        this.bookNumber = bookNumber;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.userName);
        dest.writeString(this.name);
        dest.writeString(this.lastName);
        dest.writeString(this.bookNumber);
        dest.writeString(this.passWord);
        dest.writeInt(this.idUser);
        dest.writeString(this.image);
    }

    protected User(Parcel in) {
        this.userName = in.readString();
        this.name = in.readString();
        this.lastName = in.readString();
        this.bookNumber = in.readString();
        this.passWord = in.readString();
        this.idUser = in.readInt();
        this.image = in.readString();
    }

    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
