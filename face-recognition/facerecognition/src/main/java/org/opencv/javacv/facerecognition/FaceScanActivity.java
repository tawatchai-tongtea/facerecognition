package org.opencv.javacv.facerecognition;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.objdetect.CascadeClassifier;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class FaceScanActivity extends AppCompatActivity implements CameraBridgeViewBase.CvCameraViewListener2{

    private static final int frontCam =1;
    private static final int backCam =2;

    private static final String    TAG                 = "POR";
    private static final Scalar FACE_RECT_COLOR     = new Scalar(0, 255, 0, 255);
    public static final int        JAVA_DETECTOR       = 0;
    public static final int        NATIVE_DETECTOR     = 1;

    public static final int TRAINING= 0;
    public static final int SEARCHING= 1;
    public static final int IDLE= 2;

    private Tutorial3View   mOpenCvCameraView;
    private Mat mRgba;
    private Mat                    mGray;
    private ImageView btnSwitchCamera;
    private int mChooseCamera = backCam;
    private File mCascadeFile;
    private CascadeClassifier mJavaDetector;
    private int                    mDetectorType       = JAVA_DETECTOR;
    private int                    mAbsoluteFaceSize   = 0;
    private float                  mRelativeFaceSize   = 0.2f;
    private int faceState=IDLE;

    static final long MAXIMG = 10;
    private int mLikely=999;
    PersonRecognizer fr;
    String mPath="";
    int countImages=0;
    Handler mHandler;
    Bitmap mBitmap;
    labels labelsFile;

    boolean isRecord;
    private User user;
    private Toast toast;


    private BaseLoaderCallback  mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i(TAG, "OpenCV loaded successfully");

                    // Load native library after(!) OpenCV initialization
                    //   System.loadLibrary("detection_based_tracker");



                    fr=new PersonRecognizer(mPath);
                    String s = getResources().getString(R.string.Straininig);
                    Toast.makeText(getApplicationContext(),s, Toast.LENGTH_LONG).show();
                    fr.load();


                    try {
                        // load cascade file from application resources
                        InputStream is = getResources().openRawResource(R.raw.lbpcascade_frontalface);
                        File cascadeDir = getDir("cascade", Context.MODE_PRIVATE);
                        mCascadeFile = new File(cascadeDir, "lbpcascade.xml");
                        FileOutputStream os = new FileOutputStream(mCascadeFile);

                        byte[] buffer = new byte[4096];
                        int bytesRead;
                        while ((bytesRead = is.read(buffer)) != -1) {
                            os.write(buffer, 0, bytesRead);
                        }
                        is.close();
                        os.close();

                        mJavaDetector = new CascadeClassifier(mCascadeFile.getAbsolutePath());
                        if (mJavaDetector.empty()) {
                            Log.e(TAG, "Failed to load cascade classifier");
                            mJavaDetector = null;
                        } else
                            Log.i(TAG, "Loaded cascade classifier from " + mCascadeFile.getAbsolutePath());

                        //                 mNativeDetector = new DetectionBasedTracker(mCascadeFile.getAbsolutePath(), 0);

                        cascadeDir.delete();

                    } catch (IOException e) {
                        e.printStackTrace();
                        Log.e(TAG, "Failed to load cascade. Exception thrown: " + e);
                    }

                    mOpenCvCameraView.enableView();

                    if (!fr.canPredict())
                    {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.SCanntoPredic), Toast.LENGTH_LONG).show();

                    }

                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;


            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_face_scan);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setTitle(getString(R.string.face_recognition));
        getSupportActionBar().setTitle(getString(R.string.face_recognition));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

            }
        });


        user = (User) getIntent().getExtras().getParcelable("User");

        mOpenCvCameraView = (Tutorial3View) findViewById(R.id.tutorial3_activity_java_surface_view);
        btnSwitchCamera = (ImageView)findViewById(R.id.btnSwitchCamera);
        mOpenCvCameraView.setCvCameraViewListener(this);

        mPath= Environment.getExternalStorageDirectory().toString()+"/facerecogOCV/";//getFilesDir()+"/facerecogOCV/";
        Log.w(TAG,"mPath "+mPath);
        boolean success=(new File(mPath)).mkdirs();
        if (!success)
        {
            Log.e("Error","Error creating directory");
        }
//        labelsFile= new labels(mPath);



        btnSwitchCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mChooseCamera==frontCam)
                {
                    mChooseCamera=backCam;
                    mOpenCvCameraView.setCamBack();
                }
                else
                {
                    mChooseCamera=frontCam;
                    mOpenCvCameraView.setCamFront();

                }

            }
        });

        toast = Toast.makeText(this,getString(R.string.face_not_match),Toast.LENGTH_SHORT);

        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (msg.obj=="IMG")
                {
                    Log.d(TAG,"Add image");
//                    }
                }
                else
                {
//                    textresult.setText(msg.obj.toString());

                    Log.w("POR","mLikely "+mLikely);

                    if (mLikely<0){

                    }else if (mLikely<70 && user.userName.equals(msg.obj.toString())){


                       dialogResult(msg.obj.toString());

                    }else{

//                        toast.cancel();

                        if (!fr.canPredict())
                        {
                            toast.setText(getString(R.string.SCanntoPredic));

                        }else{
                            toast.setText(getString(R.string.face_not_match));
                        }
                        toast.show();
                    }

                }
            }
        };

        faceState = SEARCHING;
    }

    @Override
    public void onCameraViewStarted(int width, int height) {

        mGray = new Mat();
        mRgba = new Mat();
    }

    @Override
    public void onCameraViewStopped() {

        mGray.release();
        mRgba.release();
    }

    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {

        mRgba = inputFrame.rgba();
        mGray = inputFrame.gray();


        if (mAbsoluteFaceSize == 0) {
            int height = mGray.rows();
            if (Math.round(height * mRelativeFaceSize) > 0) {
                mAbsoluteFaceSize = Math.round(height * mRelativeFaceSize);
            }
            //  mNativeDetector.setMinFaceSize(mAbsoluteFaceSize);
        }
//
        MatOfRect faces = new MatOfRect();

        if (mDetectorType == JAVA_DETECTOR) {
            if (mJavaDetector != null)
                mJavaDetector.detectMultiScale(mGray, faces, 1.1, 2, 2, // TODO: objdetect.CV_HAAR_SCALE_IMAGE
                        new Size(mAbsoluteFaceSize, mAbsoluteFaceSize), new Size());
        }
        else if (mDetectorType == NATIVE_DETECTOR) {
//            if (mNativeDetector != null)
//                mNativeDetector.detect(mGray, faces);
        }
        else {
            Log.e(TAG, "Detection method is not selected!");
        }

        Rect[] facesArray = faces.toArray();


        if ((facesArray.length>0) && (faceState==SEARCHING))
        {


            Mat m=new Mat();
            m=mRgba.submat(facesArray[0]);
            mBitmap = Bitmap.createBitmap(m.width(),m.height(), Bitmap.Config.ARGB_8888);


            Utils.matToBitmap(m, mBitmap);
            Message msg = new Message();
            String textTochange = "IMG";
            msg.obj = textTochange;
            mHandler.sendMessage(msg);

            textTochange=fr.predict(m);
            mLikely=fr.getProb();
            msg = new Message();
            msg.obj = textTochange;
            mHandler.sendMessage(msg);

            m.release();
            m = null;
            Log.w("POR" ,"release2");

        }
        for (int i = 0; i < facesArray.length; i++)
            Core.rectangle(mRgba, facesArray[i].tl(), facesArray[i].br(), FACE_RECT_COLOR, 3);
//
        return mRgba;

    }

    @Override
    public void onPause()
    {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();

        if (toast != null){
            toast.cancel();
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();
        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_3, this, mLoaderCallback);


    }

    public void onDestroy() {
        super.onDestroy();
        mOpenCvCameraView.disableView();
    }

    private void setMinFaceSize(float faceSize) {
        mRelativeFaceSize = faceSize;
        mAbsoluteFaceSize = 0;
    }

    void grabarOnclick()
    {
//        if (toggleButtonGrabar.isChecked())
//            faceState=TRAINING;
//        else
//        { if (faceState==TRAINING)	;
//            // train();
//            //fr.train();
//            countImages=0;
//            faceState=IDLE;
//        }


    }

    private void dialogResult(String username){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_result, null);

        ImageView imgNew = (ImageView)view.findViewById(R.id.imgNew);
        ImageView imgOld = (ImageView)view.findViewById(R.id.imgOld);
        TextView txtUsername = (TextView)view.findViewById(R.id.txtUsername);

        Log.w("POR","user.image "+user.image);

        txtUsername.setText(user.userName);
//        Picasso.with(this)
//                .load(Uri.parse(user.image))
//                .placeholder(R.drawable.ic_user)
//                .resize(200, 200)
//                .centerCrop()
//                .error(R.drawable.ic_user)
//                .into(imgOld);
        imgOld.setImageURI(Uri.parse(user.image));
        imgNew.setImageBitmap(mBitmap);

        builder.setView(view);
        builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                Intent intent = new Intent(FaceScanActivity.this,AddAccountActivity.class);
                intent.putExtra("isView",true);
                intent.putExtra("User",FaceScanActivity.this.user);
                startActivity(intent);

                finish();

                mBitmap.recycle();
                mBitmap = null;
            }
        });
        builder.create();
        builder.show();

        faceState = IDLE;
    }
}
