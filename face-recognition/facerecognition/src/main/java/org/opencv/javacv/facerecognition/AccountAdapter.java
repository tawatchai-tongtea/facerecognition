package org.opencv.javacv.facerecognition;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.orm.SugarContext;

import java.io.File;
import java.io.FilenameFilter;
import java.util.List;

/**
 * Created by tawatchaitongtea on 4/2/2016 AD.
 */
public class AccountAdapter extends RecyclerSwipeAdapter<AccountAdapter.ViewHolder> {

    private Context context;
    private List<User> users;

    public AccountAdapter(Context context){

        this.context = context;
        SugarContext.init(context);

        getUser();
    }

    private void getUser(){

        users = User.listAll(User.class);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.item_account,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        final User user = users.get(position);

        viewHolder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String mPath = context.getFilesDir()+"/facerecogOCV/";
                File root = new File(mPath);

                FilenameFilter pngFilter = new FilenameFilter() {
                    public boolean accept(File dir, String n) {
                        String s=user.userName;
                        return n.toLowerCase().startsWith(s.toLowerCase()+"-");

                    };
                };
                File[] imageFiles = root.listFiles(pngFilter);
                for (File image : imageFiles) {
                    image.delete();

                }

                user.delete();
                getUser();
                notifyDataSetChanged();
            }
        });

        viewHolder.imgProfile.setImageURI(Uri.parse(user.image));
        viewHolder.txtUsername.setText(user.userName);
        viewHolder.txtBookNumber.setText(user.bookNumber);

    }


    @Override
    public int getItemCount() {
        return users.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView txtUsername;
        public TextView txtBookNumber;
        public ImageView imgProfile;
        public FrameLayout btnDelete;

        public ViewHolder(View itemView) {
            super(itemView);

            txtUsername = (TextView)itemView.findViewById(R.id.txtUsername);
            txtBookNumber = (TextView)itemView.findViewById(R.id.txtBookNumber);
            imgProfile = (ImageView)itemView.findViewById(R.id.imgProfile);
            btnDelete = (FrameLayout)itemView.findViewById(R.id.btnDelete);


        }
    }
}
