package org.opencv.javacv.facerecognition;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.orm.SugarContext;

import org.opencv.android.Utils;
import org.opencv.core.Mat;

import java.io.File;
import java.util.List;

public class AddAccountActivity extends AppCompatActivity {

    private ImageView imgProfile;
    private FrameLayout btnAdd;

    private EditText edtUserName;
    private EditText edtName;
    private EditText edtLastName;
    private EditText edtBookNumber;
    private EditText edtPassword;
    private EditText edtConfirmPassword;
    private ImageView imgCamera;

    PersonRecognizer fr;
    String mPath;
    Mat mat;
    Bitmap mBitmap;
    boolean isView;
    private User user;

    labels labelsFile;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_account);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

            }
        });

//        Environment.getExternalStorageDirectory().toString()
        mPath= Environment.getExternalStorageDirectory().toString()+"/facerecogOCV/";//getFilesDir()+"/facerecogOCV/";
        boolean success=(new File(mPath)).mkdirs();
        if (!success)
        {
            Log.e("Error","Error creating directory");
        }

//        labelsFile= new labels(mPath);

        fr=new PersonRecognizer(mPath);
        String s = getResources().getString(R.string.Straininig);
//        Toast.makeText(getApplicationContext(),s, Toast.LENGTH_LONG).show();
        fr.load();

        imgProfile = (ImageView)findViewById(R.id.imgProfile);
        btnAdd = (FrameLayout)findViewById(R.id.btnAdd);
        edtUserName = (EditText)findViewById(R.id.edtUserName);
        edtName = (EditText)findViewById(R.id.edtName);
        edtLastName = (EditText)findViewById(R.id.edtLastName);
        edtBookNumber = (EditText)findViewById(R.id.edtBookNumber);
        edtPassword = (EditText)findViewById(R.id.edtPassword);
        edtConfirmPassword = (EditText)findViewById(R.id.edtConfirmPassword);
        imgCamera = (ImageView)findViewById(R.id.imgCamera);

        isView = getIntent().getBooleanExtra("isView",false);

        if (!isView){

            toolbar.setTitle(getString(R.string.register));
            getSupportActionBar().setTitle(getString(R.string.register));

            imgProfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    startActivityForResult(new Intent(AddAccountActivity.this,FaceRecognitionActivity.class),100);
                }
            });

            btnAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (validateFields()){

                        if (mat != null){

                            SugarContext.init(AddAccountActivity.this);

                            List<User> users = User.listAll(User.class);

                            boolean isExist = false;
                            for(User user : users){

                                Log.w("POR","user.userName "+user.userName);
                                Log.w("POR","edtUserName "+edtUserName.getText().toString());

                                if (user.userName.equals(edtUserName.getText().toString())){

                                    alertOK(getString(R.string.username_already_exist));
                                    isExist = true;
                                    break;
                                }
                            }

                            if (isExist){
                                return;
                            }
                            String pathImage =  fr.add(mat, edtUserName.getText().toString());
                            Log.w("POR","pathImage "+pathImage);


                            int idUser = users.size()+1;

                            Log.w("POR","idUser "+idUser);

                            User user = new User(edtUserName.getText().toString(),
                                    edtName.getText().toString(),
                                    edtLastName.getText().toString(),
                                    edtBookNumber.getText().toString(),
                                    edtPassword.getText().toString(),idUser,pathImage);
                            user.save();


                            finish();

                            mBitmap.recycle();
                            mBitmap = null;

                            mat.release();
                            mat = null;


                            Log.w("POR" ,"release1");

                        }else{

                            alertOK(getString(R.string.please_add_face));

                        }


                    }

                }
            });

        }else {

            user = (User) getIntent().getExtras().getParcelable("User");

            toolbar.setTitle(user.name);
            getSupportActionBar().setTitle(user.name);

            imgProfile.setImageURI(Uri.parse(user.image));

            imgCamera.setVisibility(View.GONE);
            edtPassword.setVisibility(View.GONE);
            edtConfirmPassword.setVisibility(View.GONE);
            btnAdd.setVisibility(View.GONE);

            edtName.setText(user.name);
            edtUserName.setText(user.userName);
            edtLastName.setText(user.lastName);
            edtBookNumber.setText(user.bookNumber);

            edtName.setEnabled(false);
            edtName.setFocusable(false);

            edtUserName.setEnabled(false);
            edtUserName.setFocusable(false);

            edtLastName.setEnabled(false);
            edtLastName.setFocusable(false);

            edtBookNumber.setEnabled(false);
            edtBookNumber.setFocusable(false);

        }




    }

    private void alertOK(String message){

        new  AlertDialog.Builder(AddAccountActivity.this).setTitle(getString(R.string.alert))
                .setMessage(message)
                .setPositiveButton(getString(R.string.ok),null)
                .create()
                .show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 100 && resultCode == RESULT_OK){

            mat = (Mat) data.getParcelableExtra("RecognitionBitmap");
            mBitmap = Bitmap.createBitmap(mat.width(),mat.height(), Bitmap.Config.ARGB_8888);
            Utils.matToBitmap(mat, mBitmap);

            imgProfile.setImageBitmap(mBitmap);
        }
    }

    private boolean validateFields(){

        boolean isValdate = true;

        if (edtUserName.getText().toString().isEmpty()){
            edtUserName.setError(getString(R.string.please_enter_username));
            isValdate = false;
        }else if (edtName.getText().toString().isEmpty()){
            edtName.setError(getString(R.string.please_enter_name));
            isValdate = false;
        }else if (edtLastName.getText().toString().isEmpty()){
            edtLastName.setError(getString(R.string.please_enter_last_name));
            isValdate = false;
        }else if (edtBookNumber.getText().toString().isEmpty()){
            edtBookNumber.setError(getString(R.string.please_enter_book_number));
            isValdate = false;
        }else if (edtPassword.getText().toString().isEmpty()){
            edtPassword.setError(getString(R.string.please_enter_password));
            isValdate = false;
        }else if (!edtPassword.getText().toString().equals(edtConfirmPassword.getText().toString())){
            edtConfirmPassword.setError(getString(R.string.confirm_password_not_match));
            isValdate = false;
        }
        return isValdate;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        if (isView){

            MenuInflater menuInflater = getMenuInflater();
            menuInflater.inflate(R.menu.menu_add_account,menu);

            if (!user.userName.equals(getString(R.string.admin))){
                menu.getItem(0).setVisible(false);
            }


            return true;
        }else{

            return false;
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.list_account:

                startActivity(new Intent(AddAccountActivity.this,AccountListActivity.class));
                return true;
            case R.id.logout:

                finish();
                return true;

        }

        return super.onOptionsItemSelected(item);
    }
}
