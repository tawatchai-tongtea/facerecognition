package org.opencv.javacv.facerecognition;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;

import com.orm.SugarContext;

import java.util.List;

public class LoginActivity extends AppCompatActivity {

    private User user;
    EditText edtUserName;
    EditText edtPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                100);

        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                100);

        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.CAMERA},
                100);

        FrameLayout btnLogin = (FrameLayout)findViewById(R.id.btnLogin);
        FrameLayout btnRegister = (FrameLayout)findViewById(R.id.btnRegister);

        edtUserName = (EditText)findViewById(R.id.edtUserName);
        edtPassword = (EditText)findViewById(R.id.edtPassword);



        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SugarContext.init(LoginActivity.this);

                List<User> users = User.listAll(User.class);

                boolean isExist = false;
                for(User user : users){

                    if (user.userName.equals(edtUserName.getText().toString()) && user.passWord.equals(edtPassword.getText().toString())){

                        isExist = true;
                        LoginActivity.this.user = user;
                        break;
                    }
                }


                if (isExist){


                    Intent intent = new Intent(LoginActivity.this,FaceScanActivity.class);
                    intent.putExtra("User",LoginActivity.this.user);
                    startActivity(intent);
                }else {

                    alertOK(getString(R.string.login_fail));

                }

            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(LoginActivity.this,AddAccountActivity.class);
                intent.putExtra("isView",false);
                startActivity(intent);
            }
        });
    }

    private void alertOK(String message){

        new  AlertDialog.Builder(LoginActivity.this).setTitle(getString(R.string.alert))
                .setMessage(message)
                .setPositiveButton(getString(R.string.ok),null)
                .create()
                .show();
    }

    @Override
    protected void onResume() {
        super.onResume();

        edtUserName.requestFocus();
        edtUserName.setText("");
        edtPassword.setText("");
    }
}
